use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(Config, attributes(configuro))]
pub fn derive_config(input: TokenStream) -> TokenStream {
    handle_derive(input, "./config.msgpack")
}

#[proc_macro_derive(Data, attributes(configuro))]
pub fn derive_data(input: TokenStream) -> TokenStream {
    handle_derive(input, "./data.msgpack")
}

fn handle_derive(input: TokenStream, path: &str) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let ident = &input.ident;
    let (_impl_gen, type_gen, where_clause) = &input.generics.split_for_impl();

    quote::quote! {
        use configuro::Configuro;
        impl<'a> configuro::Configuro<'a> for #ident #type_gen #where_clause {
            const PATH: &'a str = #path;

            fn read() -> Self {
                let json = std::fs::read(Self::PATH);
                if json.is_err() {
                    return Self::default();
                }


                rmp_serde::from_slice::<Self>(&json.unwrap()).unwrap()
            }

            fn write(&self) {
                let json = rmp_serde::to_vec(self);
                if json.is_err() {
                    return;
                }

                let _ = std::fs::write(Self::PATH, json.unwrap());
            }
        }

        impl Drop for #ident #type_gen #where_clause {
            fn drop(&mut self) {
                self.write();
            }
        }
    }
    .into()
}
