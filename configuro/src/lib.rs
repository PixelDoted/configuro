#[cfg(feature = "derive")]
pub use configuro_derive::*;
pub extern crate rmp_serde;

pub trait Configuro<'a> {
    const PATH: &'a str;

    fn read() -> Self;
    fn write(&self);
}
