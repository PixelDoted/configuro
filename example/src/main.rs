use configuro::rmp_serde;

fn main() {
    Example {
        example: String::from("Hello, World!"),
    };
}

#[derive(serde::Deserialize, serde::Serialize, Default, configuro::Config)]
struct Example {
    pub example: String,
}
